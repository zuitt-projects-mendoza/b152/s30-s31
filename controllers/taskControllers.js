const Task = require("../models/Task")

module.exports.createTaskController = (req,res) => {

	//Check your request body first:
	console.log(req.body)

	//Task model is a constructor.
	//newTask that will be created from our Task model will have additional methods
	//to be used in our application

	/*
		Take Home Mini-activity:

		Add a validation that will disallow the creation of Task documents with the same name.

		Add a findOne() method using the appropriate model to search/query for item/document that match the req.body.name.

		Then, inside the then() add an if-else to check if the result of findOne is not null and if the name property of the result is the same as req.body.name. 
			-If it is, send a message to the client: "Duplicate Task Found"

		Else, create the new task document and use save() method to save it in our collection.

	*/

	Task.findOne({name:req.body.name})
	.then(result => {

		if(result !== null && result.name === req.body.name){
			return res.send("Duplicate Task Found")
		} else { 

			let newTask = new Task({

				name: req.body.name,
				status: req.body.status

			})

			newTask.save()
			.then(result => res.send(result))
			.catch(error => res.send(error))
		}
	})

	.catch(error => res.send(error))

	//.save() is a method from an object created by a model.
	//This will allow us to save the document into the collection.
	//save() can have anonymous function or we can have what we call a then chain:

	//The anonymous function in the save() method is used to handle the error or the proper result/response from mongodb.
	/*
		newTask.save((savedTask,error)=>{
	
			if(error){
				res.send(error)
			} else {
				res.send(savedTask)
			}

		})

	*/

	//.then and .catch chain:
	//.then() is used to handle the proper result/returned value of a function. If the function properly returns a value, we can run a separate function to handle it.
	//.catch() is used to handle/catch the error from the use of a function. So that if there is an error, we can properly handle it separate from the result.


}

module.exports.getAllTasksController = (req,res) => {

	//To be able to query with a collection, we use the Task model and its find() function. This find() function is similar to mongoDB's own find(). This will allow us to connect to our collection and retrieve all documents that matches our criteria.
	//Similar to db.tasks.find({})
	Task.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

};

module.exports.getSingleTaskController = (req,res) => {

	console.log(req.params)
	Task.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.updateTaskStatusController = (req,res) => {

	console.log(req.params.id);

	let updates = {

		status: req.body.status
	}

	Task.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error))

}