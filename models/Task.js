const mongoose = require("mongoose");
/*
	The naming convention for model files, are singular and capitalized names that describe the schema.
*/
//Mongoose Schema

//Before we can create documents from our api to save into our database, we first have to determine the structure of the documents to be written in the database.
//Schema acts as a blueprint for our data/document.
//A schema is a representation of how the document is structured. It also determines the types of data and the expected properties. Gone are the days were we have to worry if you input "stock" or "stocks" as fields. Schemas allow us disallow us to create documents which does not follow the schema:

//Schema() is a constructor from mongoose that will allow us to create a new schema object.

const taskSchema = new mongoose.Schema({
	/*
		Define the fields for the task document.
		The task document should have a name field and a status field.
		Both fields must be strings.
	*/
	name: String,
	status: String

});

//Mongoose Model
/*
	Models are used to connect your api to the corresponding collection in your database. It is a representation of the Task documents.

	Models uses schemas to create objects that correspond to the schema. By default, when creating the collection from your model, the collection name is pluralized.

	mongoose.model(<nameOfCollectionInAtlas>,<schemaToFollow>)
*/

module.exports = mongoose.model("Task",taskSchema);

//module.exports will allow us to export files/functions and be able to import/require them in another file within our application.
//Export the model into other files that may require it.